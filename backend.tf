terraform {
  backend "s3" {
    bucket = "terraform-sidiney-challenge"
    key    = "tfstate"
    region = "eu-central-1"
  }
}